package com.example.houyhyongspringhomework001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HouyhyongSpringHomework001Application {

    public static void main(String[] args) {
        SpringApplication.run(HouyhyongSpringHomework001Application.class, args);
        // All operations are in Customer Controller
    }

}
