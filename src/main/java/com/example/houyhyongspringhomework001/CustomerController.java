package com.example.houyhyongspringhomework001;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {

    //create ArrayList
    ArrayList<Customer> customers = new ArrayList<>();

    static int customerID = 3;

    //add customer
    public CustomerController(){
        customers.add(new Customer(1, "Houy Hyong", "Female", 22, "Phnom Penh"));
        customers.add(new Customer(2, "Victor Qin", "Male", 26, "China"));
        customers.add(new Customer(3, "Dylan Wang", "Male", 24, "China"));
    }

// b)	Create RestAPi with CRUD and more operation

    // i)   Insert Customer   ----------------------------------------------

        // 1- Simple insert
        /*
            @PostMapping("")
            public Customer insertCustomer (@RequestBody Customer customer) {
                customers.add(customer);
                return customer;
            }
         */

        // 2- Using RequestBody (name, gender, address)
        /*
            @PostMapping("")
            public Customer insertCustomer (@RequestBody CustomerRequest customerRequest){

                Customer insertCustomer = new Customer(
                        ++customerID,
                        new CustomerRequest(
                                customerRequest.getName(),
                                customerRequest.getGender(),
                                customerRequest.getAge(),
                                customerRequest.getAddress()
                ));
                customers.add(insertCustomer);
                return insertCustomer;
            }
         */

        // 3- Using RequestBody (name, gender, address) and Using custom ResponseEntity
        @PostMapping("")
        public ResponseEntity<CustomerResponse> insertCustomer (@RequestBody CustomerRequest customerRequest){

            Customer insertCustomer = new Customer(
                    ++customerID,
                    new CustomerRequest(
                            customerRequest.getName(),
                            customerRequest.getGender(),
                            customerRequest.getAge(),
                            customerRequest.getAddress()
                    )
            );
            customers.add(insertCustomer);

            return ResponseEntity.ok(
                    new CustomerResponse<>(
                            "This record was successfully created",
                            insertCustomer,
                            "OK",
                            LocalDateTime.now()
                    )
            );
        }

    // i)   end of Insert Customer ---------------------------------

//-----------------------------------------------------------------------------------


    // ii)	Read/Get all customer ---------------------------------

        // 1- Simple show/get all customers
        /*
            @GetMapping("")
            public ArrayList<Customer> AllCustomer(){
                return customers;
            }
        */

        // 2- Using custom responseEntity to show all customer
        @GetMapping("")
        public ResponseEntity<CustomerResponse> readAllCustomer(){
            return ResponseEntity.ok(
                    new CustomerResponse<ArrayList<Customer>>(
                            "You get all customer successfully",
                            customers,
                            "OK",
                            LocalDateTime.now()
                    )
            );
        }
    // ii)	end of Read all customer ----------------------------

//-----------------------------------------------------------------------------------


    // iii) Read customer by id (use with @PathVariable) --------

        // 1- Simple Read/get customer by id
        /*
            @GetMapping("/{customerId}")
            public Customer getCustomerByID(@PathVariable("customerId") Integer cusID){
                for (Customer cus : customers){
                    if(cus.getId() == cusID){
                        return cus;
                    }
                }
                return null;
            }
         */

        // 2- Using custom ResponseEntity to Read/get customer by id
        @GetMapping("/{customerId}")
        public ResponseEntity<CustomerResponse> getCustomerByID(@PathVariable("customerId") Integer cusID){
            for (Customer cus : customers){
                if(cus.getId() == cusID){
                    return ResponseEntity.ok(
                            new CustomerResponse(
                                    "This record has found successfully",
                                    cus,
                                    "OK",
                                    LocalDateTime.now()
                            )
                    );
                }
            }
            return ResponseEntity.notFound().build(); //if not found, output 404
        }

    // iii) end of Read customer by id (use with @PathVariable) --------

//-----------------------------------------------------------------------------------


    // iv)	Read customer by name (use with @RequestParam) -------------

        // 1- Read/Search customer by name (simple output)
        /*
            @GetMapping("/search")
            public Customer searchCustomerByName(@RequestParam String name){
                for (Customer cus : customers){
                    if(cus.getName().equals(name)){
                        return cus;
                    }
                }
                return null;
            }
         */

        // 2- Using custom ResponseEntity to Read/Search customer by name
        @GetMapping("/search")
        public ResponseEntity<CustomerResponse> searchCustomerByName(@RequestParam String name){
            for (Customer cus : customers){
                if(cus.getName().equals(name)){
                    return ResponseEntity.ok(
                            new CustomerResponse(
                                    "This record has found successfully",
                                    cus,
                                    "OK",
                                    LocalDateTime.now()
                            )
                    );
                }
            }
            return ResponseEntity.notFound().build();
        }
    // iv)	end of Read customer by name (use with @RequestParam) -------------

//-----------------------------------------------------------------------------------


    //v)	Update customer by id ---------------------

        // 1- Update customer by id (simple output)
        /*
            @PutMapping("/{customerId}")
            public Customer UpdateCustomerById(@RequestBody CustomerRequest customerRequest, @PathVariable("customerId") Integer cusID){
                for (Customer cus : customers){
                    if(cus.getId() == cusID){
                        customers.set(cusID -1 ,
                                new Customer(
                                        cusID,
                                        new CustomerRequest( //get the name, gender, age, address from customerRequest(input in requestBody)
                                                customerRequest.getName(),
                                                customerRequest.getGender(),
                                                customerRequest.getAge(),
                                                customerRequest.getAddress()
                                        )
                                )
                        );
                        return cus;
                    }
                }
                return null;
            }
         */

        // 2- Using custom ResponseEntity to Update customer (by id)
        @PutMapping("/{customerId}")
        public ResponseEntity<CustomerResponse> UpdateCustomerById (@RequestBody CustomerRequest customerRequest, @PathVariable("customerId") Integer cusID){

            for (Customer cus : customers){
                if(cus.getId() == cusID){
                    //set updated customer
                    customers.set(cusID -1 ,
                            new Customer(
                                    cusID,
                                    new CustomerRequest( //get the name, gender, age, address from customerRequest(input in requestBody)
                                            customerRequest.getName(),
                                            customerRequest.getGender(),
                                            customerRequest.getAge(),
                                            customerRequest.getAddress()
                                    )
                            )
                    );

                    //then return with responseEntity
                    return ResponseEntity.ok(
                           new CustomerResponse(
                                    "You're update succesfully",
                                    customers,
                                    "OK",
                                    LocalDateTime.now()
                           )
                    );
                }
            }
            //else error 404
            return ResponseEntity.notFound().build();
        }
    //v)	end of Update customer by id -----------------------------

//-----------------------------------------------------------------------------------


    //vi)	Delete customer by id --------------------------

        // 1- Simple Output (Delete customer by id)
        /*
            @DeleteMapping("/{customerId}")
            public Customer DeleteCustomerById(@PathVariable("customerId") Integer cusID){
                for (Customer cus : customers){
                    if(cus.getId() == cusID){
                        customers.remove(cusID - 1);
                        return cus;
                    }
                }
                return null;
            }
         */

        // 2- Using custom ResponseEntity to Delete (customer by id)
        @DeleteMapping("/{customerId}")
        public ResponseEntity<CustomerResponse> DeleteCustomerById(@PathVariable("customerId") Integer cusID){
            for (Customer cus : customers){
                if(cus.getId() == cusID){
                    customers.remove(cusID - 1);
                    return ResponseEntity.ok(
                            new CustomerResponse(
                                    "Congratulation your delete is successfully",
                                    "OK",
                                    LocalDateTime.now()
                            )
                    );
                }
            }


            return ResponseEntity.notFound().build();
        }
    //vi)	end of Delete customer by id --------------------------

//-----------------------------------------------------------------------------------
}
